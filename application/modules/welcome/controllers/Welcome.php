<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public $autoload = array(
        'helper'    => array('url', 'form'),
        'libraries' => array('email'),
    );
	public function __construct(){
		parent::__construct();
	}

	public function index()
	{
		 $data['title'] = 'hello world';
		 $data['array'] = array('test'=>1,'demo'=>2);
        $this->smarty->view('example.tpl',$data);
	}
}
